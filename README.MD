# A spider and VITS dataset maker for **Revue Starlight: Re LIVE players**

> ATTENTION
>
> USE THIS SOFTWARE MEANS YOU AGREE WITH THE FOLLOWING LICENSE:
>
> MIT LICENSE.

## 感谢

我必须要把感谢放在最前边。没有这个网站就没有这个项目。

[https://karth.top/endpoints](https://karth.top/endpoints)

## 注意

使用本脚本的合理性和合规性存疑。程序作者不承担任何责任。使用本程序造成的一切后果由程序使用者
承担。

本程序在MIT协议下发布，你**有**一些权利，你也**没有**一些权利。请参考MIT协议原本。

## 用处

这是一个临时项目，简而言之是一个工作流。这个工作流可以根据您提供的角色ID来下载您所需要的角色数据。这里一共有几个故事，存放点：main/adventure.json. 我这里仅仅处理了前两个故事和10个角色（包括99期生主角团9人和**长颈鹿**）。

## 流程

1. 使用`download_text_json.py`来对各个故事的章节进行下载。下载的文件会保存到`story/`
2. 使用`train_txt_gen.py`生成角色的训练集参考。训练集参考文本格式参照于 _VITS_ 项目。这一步你需要选择您所需要设置的角色。我这里选择了9人+长颈鹿。长颈鹿分配到0号人物。
3. 使用`download_audio.py`下载所需音频文件。它会根据上一步生成的训练集文本部分自动下载所需的音频文件到`dataset/`目录下。