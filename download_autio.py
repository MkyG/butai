import httpx
import time
import os


with open("output.txt",encoding="utf-8") as f:
    lines = f.readlines()

for line in lines:
    audio_name = line.split("|")[0].split("/")[1]
    
    if os.path.exists("dataset/"+audio_name):
        print(audio_name,"exists")
        continue
    
    id1 = audio_name.split(".")[0][1:3]
    id2 = audio_name.split(".")[0][3:5]
    
    story_id = audio_name.split(".")[0][0]
    
    match story_id:
        case "m":
            url = f"https://cdn.karth.top/api/assets/dlc/res/scenario/voice/1000{id1}00{id2}/{audio_name}"
        case "s":
            url = f"https://cdn.karth.top/api/assets/dlc/res/scenario/voice/2000{id1}00{id2}/{audio_name}"
        case _:
            continue
    
    for _ in range(5):
        try:
            r = httpx.get(url)
            break
        except:
            time.sleep(2)
            continue
    with open("dataset/"+audio_name,"wb") as f:
        f.write(r.content)
    
    print(audio_name,"done")
    time.sleep(3)