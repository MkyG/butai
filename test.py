import json

NAME_JSON = json.load(open("name.json",encoding="utf-8"))

def get_name_from_id(id: int) -> str:
    return NAME_JSON[str(id)]["ja"]

SCRIPT = json.load(open("1000010001.json",encoding="utf-8"))
SCRIPT = SCRIPT['script']

for item in SCRIPT:
    try:
        i = int(item)
    except:
        continue
    
    if SCRIPT[item]["type"] != "message":
        continue
    
    speakerId = SCRIPT[item]["args"]["nameId"]
    speakerName = get_name_from_id(speakerId)
    
    body = SCRIPT[item]["args"]["body"]
    body = body.replace("\\n"," ").replace("\n"," ")
    
    voiceId = SCRIPT[item]["args"]["voiceId"]
    print(f"{voiceId} {speakerName}:\t{body}")