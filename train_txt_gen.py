import json
import httpx

# Parse the main story json file and generate the output.txt file
# an output.txt file is like this:
# dataset/m1812_059.wav|7|純那ちゃん……。
#
# When you wanna enable a new character, 
# you should add the character's id to SELECTED_NAME_ID
# and download the audio files by download_audio.py
#
# Copyrigth (C) 2023 by Merky
# This program is free software: you can redistribute it and/or modify under MIT License


ADV = json.load(open("main/adventure.json",encoding="utf-8"))
MAIN_STORY = ADV["main_story"]

NAME_JSON = json.load(open("main/name.json",encoding="utf-8"))

def get_name_from_id(id: int) -> str:
    return NAME_JSON[str(id)]["ja"]

SELECTED_NAME_ID = ['101','102','103','104','105',
                    '106','107','108','109','801']
    
def gen_train_txt(story, append: bool = False):
    if append:
        fileflag = "a"
    else:
        fileflag = "w"
    fw = open("output.txt",fileflag,encoding="utf-8")

    for essay in story:
        story_id = story[essay]["id"]
        story_item = json.load(open(f"story/{story_id}.json",encoding="utf-8"))
        script = story_item["script"]
        for item in script:
            try:
                i = int(item)
            except:
                continue
            
            script_item = script[item]
            if script_item["type"] != "message":
                continue
            
            speakerId = script_item["args"]["nameId"]
            if str(speakerId) not in SELECTED_NAME_ID:
                continue
            
            try:
                speakerName = get_name_from_id(speakerId)
            except:
                continue
            
            if speakerId == 801: # 801 is the id of Giraffe
                speakerId = 100
            body = script_item["args"]["body"]
            body = body.replace("\\n"," ").replace("\n"," ")
            
            try:
                voiceId = script_item["args"]["voiceId"]
            except:
                pass
            print(f"{voiceId} {speakerName}:\t{body}")
            
            fw_str = f"dataset/{voiceId}.wav|{speakerId-100}|{body}\n"
            fw.write(fw_str)

    fw.close()
    
gen_train_txt(MAIN_STORY)
gen_train_txt(ADV['academy_story'],append=True)