import json
import httpx

# Parse the main story json file and generate the output.txt file
# an output.txt file is like this:
# dataset/m1812_059.wav|7|純那ちゃん……。
#
# When you wanna enable a new character, 
# you should add the character's id to SELECTED_NAME_ID
# and download the audio files by download_audio.py
#
# Copyrigth (C) 2023 by Merky
# This program is free software: you can redistribute it and/or modify under MIT License


ADV = json.load(open("main/adventure.json",encoding="utf-8"))
story = ADV["main_story"]

NAME_JSON = json.load(open("main/name.json",encoding="utf-8"))

def get_name_from_id(id: int) -> str:
    return NAME_JSON[str(id)]["ja"]

SELECTED_NAME_ID = ['101','102','103','104','105',
                    '106','107','108','109','801']

# Download all the story json files from Internet
def download_story_json(story):
    for essay in story:
        story_id = story[essay]["id"]
        print(story_id,story[essay]["title"]["ja"])
        url = f"https://karth.top/api/adventure/jp/{story_id}.json"
        r = httpx.get(url)
        with open(f"story/{story_id}.json","w",encoding="utf-8") as f:
            f.write(r.text)


download_story_json(ADV['academy_story'])
    